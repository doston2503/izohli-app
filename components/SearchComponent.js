import React, {useEffect} from 'react';
import {useRouter} from 'next/router';
import {connect} from "react-redux";
import {getWords, updateState} from "../redux/actions/mainAction";

function SearchComponent(props) {
    const router = useRouter();

    // function searchWord(e) {
    //     props.updateState({searchWord: e.target.value || ''});
    //     if (e.target.value === '') {
    //         props.updateState({words: []});
    //         router.push("/")
    //     } else {
    //         if (e.target.value?.length>1){
    //             props.getWords(e.target?.value || '', false)
    //         }
    //
    //     }
    // }

    let timeoutId;

    function delayedSearchWord(e) {
        props.updateState({searchWord: e.target.value});
        if (e.target.value === '') {
            props.updateState({ words: [] });
            router.push("/");
        }
            clearTimeout(timeoutId);
            timeoutId = setTimeout(() => {
                if (e.target.value?.length > 1) {
                    props.getWords(e.target?.value || '', false);
                }
            }, 2000);
    }


    useEffect(() => {
        if (!router.pathname.includes('/search/')) {
            props.updateState({words: [], searchWord: ''})
        }
        return () => {
            props.updateState({words: [], searchWord: ''})
        }
    }, [router.pathname]);

    return (
        <>
            <div className="search-component">
                <div className="search-area">
                    <button type={'submit'} className="search-icon">
                        <img src="/images/search.svg" alt=""/>
                    </button>
                    <input
                        value={props.searchWord}
                        onChange={delayedSearchWord}
                        placeholder="Soʻzni izlash ..."
                        id="searchInput"
                        name="searchInput"
                        className="search-input"
                        type="search"
                    />
                </div>
            </div>
            {
                props.words?.length < 1 ? '' :
                    <ul className="similar-words">
                        <li className="text">Oʻxshash soʻzlar</li>
                        {!props.words ? <div style={{fontSize:"20px"}}>Bunday soʻz topilmadi</div>
                            :  props.words?.map((item, index) => (
                                <li key={index}
                                    onClick={() => {
                                        props.updateState({searchWord: item.label});
                                        router.push('/search/' + item.label);
                                    }}
                                    className={props.searchWord===item?.label ? 'active-list':''}
                                    style={{cursor: 'pointer'}}>#{item?.label}</li>
                            ))
                        }

                    </ul>
            }
        </>

    );
}

const mapStateToProps = state => {
    return {
        words: state.main.words,
        searchWord: state.main.searchWord,
    }
};

export default connect(mapStateToProps, {getWords, updateState})(SearchComponent);
