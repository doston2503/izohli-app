import React, {useState} from 'react';
import {formatDate} from "../src/utils/constants";

function MainWord({dayWords}) {
    const [collapseId, setCollapseId] = useState(null);
    return (
        <div className="main-word-component">
            <div className="content-header">
                <div className="title">
                    <div className="word-name">
                        KUN SOʻZI
                        <span>{formatDate(dayWords?.date)}</span>
                    </div>
                    {/*<div className="voice-image">
                        <img src="/images/voice.svg" alt="" />
                    </div>*/}
                </div>
                <div className="text">{dayWords?.words?.label}</div>
                <div className="text-accent">
                    [ {dayWords?.words?.transcript} ] <i>{dayWords?.words?.categoryName}</i>
                    {/*<img src="/images/star.svg" alt="" />*/}
                </div>

            </div>

            <hr />

            {dayWords?.words?.notes?.map((item,index)=>(
                <div className="content-body" key={index}>
                    <span className="d-flex">{index+1}.
                       <div dangerouslySetInnerHTML={{__html: item?.titles }}/>
                        </span>
                    <div style={{marginLeft:"28px"}}
                         dangerouslySetInnerHTML={{__html: item?.descriptions ? item?.descriptions:''}}>
                    </div>
                    <div style={{marginLeft:"28px"}}>
                        <div className="source">Manba</div>
                        <div className="source-text">
                            <i dangerouslySetInnerHTML={{__html: item?.sources}}/>
                        </div>
                    </div>
                </div>
            ))}

            {dayWords?.words?.wordTypes ?
            <div className="border-top mt-4 pt-4" >
                {dayWords?.words?.wordTypes?.map((type, index) => (
                    <div className="d-flex align-items-center mb-3" key={index}>
                        <b className="d-block" style={{fontSize:'18px'}}>{type?.typeName}</b>:
                        {type?.wordIds?.map((word, wordIndex) => (
                            <div key={wordIndex} className="ms-2">
                                <a className="text-decoration-none"
                                   style={{fontSize:'18px'}}
                                   target="_blank"
                                   href={`/search/${word}`}>{word}</a>
                                {wordIndex === type?.wordIds?.length - 1 ? null : ','}
                            </div>
                        ))}</div>
                ))}
            </div>
            :''}

        </div>
    );
}

export default MainWord;
