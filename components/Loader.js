import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {updateState} from "../redux/actions/mainAction";

const Loader = (props) => {
    useEffect(() => {
        if (props.isLoading) {
            document.body.style.overflow = 'hidden';
        } else {
            document.body.style.overflow = 'auto';
        }

        return () => {
            document.body.style.overflow = 'auto';
        };
    }, [props.isLoading]);
    return (
        <div className="loader-style">
            <div className="loader">
                <span>I</span>
                <span>Z</span>
                <span>O</span>
                <span>H</span>
                <span>L</span>
                <span>I</span>
            </div>

        </div>
    );
};

const mapStateToProps = state => {
    return {
        isLoading: state.main.isLoading,
    }
};

export default connect(mapStateToProps, {updateState})(Loader);