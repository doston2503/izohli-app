import React from 'react';

const Faq = (props) => {
    return (
        <div className="faq-component">
            <div className="title">
                Lugʻatdan qanday foydalaniladi ?
            </div>
            <p className="text">
                Tekshirmoqchi boʻlgan soʻzingizni izlash satriga kiriting va Izlash tugmasini bosing.
                Qidiruv natijalari izlash satri ostida roʻyxat qilib koʻrsatiladi. Roʻyxatdagi biror soʻz ustiga
                bossangiz uning izohini koʻrishingiz mumkin.
            </p>

            <div className="download-mobile-app">
                <p>Izohli ilovasini hoziroq yuklab oling</p>
                <a href="#">
                    <img src="/images/app-store.svg" alt=""/>
                </a>
                <a href="#" className="ms-4">
                    <img src="/images/google-play.svg" alt=""/>
                </a>
            </div>
        </div>
    );
};

export default Faq;
