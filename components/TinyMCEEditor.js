import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

const TinyMCEEditor = ({ initialValue, handleEditorChange }) => {
    return (
        <Editor
            apiKey="riafc17j8myy0ofr6kcigeqyovhsgmuf1ajplp2sgq6bheee"
            initialValue={initialValue}
            init={{
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount',
                ],
                toolbar:
                    'undo redo | formatselect | ' +
                    'bold italic backcolor | alignleft aligncenter ' +
                    'alignright alignjustify | bullist numlist outdent indent | ' +
                    'removeformat | help',
            }}
            onEditorChange={handleEditorChange}
        />
    );
};

export default TinyMCEEditor;