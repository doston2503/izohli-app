import React, {useEffect} from 'react';
import Link from "next/link";
import {useRouter} from "next/router";
import axios from "axios";
import {PATH_NAME} from "../src/utils/constants";
import {toast} from "react-toastify";

function AdminLayout({children}) {
    const router = useRouter();
    const currentPath = router.pathname;

    function logOut() {
        let user = JSON.parse(localStorage.getItem('user'));
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        const postData = {
            username: user.username,
            password: user.password,
        };

        axios.post(`${PATH_NAME}users/logout`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    localStorage.clear();
                    router.push('/login')

                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        let date = new Date().getTime();
        const token = localStorage.getItem('accessToken');
        let timestamp = localStorage.getItem('timestamp');
        let status = localStorage.getItem('status');
        timestamp = new Date(timestamp).getTime();

        if (typeof window !== 'undefined') {
            if (!token || date > timestamp || !status) {
                localStorage.clear();
                router.push('/login');
            } else {
                // return children
            }
        }


    }, []);

    return (
        <div className="admin-layout">
            <div className="admin-layout-left">
                <Link href="/" className="mainBrand">
                    <img src="/images/logo.svg" alt=""/>
                </Link>

                <ul>
                    <li><Link className={currentPath === '/admin/category' ? 'active-link' : ''}
                              href='/admin/category'>Kategoriya</Link></li>
                    <li><Link className={currentPath === '/admin/words' ? 'active-link' : ''}
                              href='/admin/words'>Soʻzlar</Link></li>
                    <li><Link className={currentPath === '/admin/types' ? 'active-link' : ''}
                              href='/admin/types'>Soʻz turlari</Link></li>
                    <li><Link className={currentPath === '/admin/word-type' ? 'active-link' : ''}
                              href='/admin/word-type'>Soʻzlarga soʻz turi qoʻshish</Link></li>
                    <li><Link className={currentPath === '/admin/note' ? 'active-link' : ''}
                              href='/admin/note'>Qoʻshimcha maʻlumot</Link></li>
                    {/*<li><Link className={currentPath === '/admin/sentence' ? 'active-link' : ''}*/}
                    {/*          href='/admin/sentence'>Gaplar</Link></li>*/}
                    {/*<li><Link className={currentPath === '/admin/word-in-sentence' ? 'active-link' : ''}*/}
                    {/*          href='/admin/word-in-sentence'>Soʻzga oid gaplar</Link></li>*/}
                    <li><Link className={currentPath === '/admin/day-words' ? 'active-link' : ''}
                              href='/admin/day-words'>Kun soʻzi</Link></li>
                    <li><Link className={currentPath === '/admin/contact' ? 'active-link' : ''}
                              href='/admin/contact'>Bogʻlanish</Link></li>
                    <li><Link className={currentPath === '/admin/upload-all-data' ? 'active-link' : ''}
                              href='/admin/upload-all-data'>Barcha maʻlumotlarni yuklash</Link></li>

                    <li onClick={logOut} style={{marginTop:"100px"}}><Link
                                               href='#'>Chiqish</Link></li>
                </ul>

            </div>
            <div className="admin-layout-right">
                <div className="admin-layout-content">
                    <main>{children}</main>
                </div>
            </div>
        </div>
    );
}

export default AdminLayout;