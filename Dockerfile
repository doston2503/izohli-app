
FROM node:18.8 as dependencies

WORKDIR /app
COPY package.json ./
RUN npm i --force

FROM node:18.8 as builder
WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules ./node_modules
COPY --from=dependencies /etc/resolv.conf ./docker/resolv.conf
RUN npm run build

FROM node:18.8 as runner
WORKDIR /app
ENV NODE_ENV production
# If you are using a custom next.config.js file, uncomment this line.
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

EXPOSE ${PORT}
CMD ["npm", "start"]
