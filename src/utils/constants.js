export const PATH_NAME = 'http://185.196.213.14:8003/';

const uzbekMonthNames = [
    'Yanvar',
    'Fevral',
    'Mart',
    'Aprel',
    'May',
    'Iyun',
    'Iyul',
    'Avgust',
    'Sentabr',
    'Oktabr',
    'Noyabr',
    'Dekabr',
];

export function formatDate(inputDate) {
    const date = new Date(inputDate);
    // const options = {year: 'numeric', month: 'long', day: 'numeric'};
    // return date.toLocaleDateString('uz-UZ', options);

    const month = date.getMonth();
    const day = date.getDate();
    const year = date.getFullYear();

    return `${uzbekMonthNames[month]} ${day}, ${year}`;


}

export function formatDateWithoutYear(inputDate) {
    const date = new Date(inputDate);
    // const options = {month: 'long', day: 'numeric'};
    // return date.toLocaleDateString('en-US', options);
    const month = date.getMonth();
    const day = date.getDate();
    return `${uzbekMonthNames[month]} ${day}`;
}
