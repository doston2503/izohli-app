import React, {useEffect, useState} from 'react';
import AdminLayout from "../../../components/AdminLayout";
import axios from "axios";
import {PATH_NAME} from "../../../src/utils/constants";
import {toast} from "react-toastify";
import Head from "next/head";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Select from 'react-select';
import {useRouter} from "next/router";
import ReactPaginate from "react-paginate";

function Index(props) {
    const router = useRouter();
    const [isOpen, setIsOpen] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);

    const [selectedOption, setSelectedOption] = useState(null);
    const [selectedOptionMulti, setSelectedOptionMulti] = useState(null);
    const [types, setTypes] = useState([]);
    const [words, setWords] = useState([]);
    const [wordsUpdate, setWordsUpdate] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const [wordsTypes, setWordsTypes] = useState([]);
    const [updateWordTypeElement, setUpdateWordTypeElement] = useState({});
    const [wordTypeId, setWordTypeId] = useState(null);
    const [wordName, setWordName] = useState("");

    const toggleModal = () => {
        setIsOpen(!isOpen);
    };
    const toggleDeleteModal = () => {
        setDeleteModal(!deleteModal);
    };
    const toggleUpdateModal = () => {
        setUpdateModal(!updateModal);
    };
    const handleChange = (selectedOption) => {
        setSelectedOption(selectedOption);
    };
    const handleInputChange = (input) => {
        setSearchInput(input);
        if (input?.length >= 2) {
            const headers = {
                headers: {
                    'Accept': 'application/json',
                },
            };
            axios.get(`${PATH_NAME}words/get-by-label-on-array?label=${input}`, headers)
                .then((response) => {
                    setWords(response.data.data);
                })
                .catch((error) => {
                    toast.error('Error');
                    console.error('Error:', error);
                });
        } else {
            setWords([])
        }

    };

    const handleReset = () => {
        setSelectedOption(null);
    };
    const handleResetUpdate = () => {
        handleChange(null);
        setSelectedOption(null);
        setUpdateWordTypeElement({});
    };


    const handleChangeMulti = (selectedOptionMulti) => {
        setSelectedOptionMulti(selectedOptionMulti);
    };

    function getAllType() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}types/get-all`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setTypes(response.data.data);
                }
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    function getAllWords() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}words/get-all`, headers)
            .then((response) => {
                setWordsUpdate(response.data.data);
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    function getAllWordType() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}word-type/search-word-type?page=0&size=10&wordName=`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setWordsTypes(response.data.data);
                }
            })
            .catch((error) => {
                toast.error('Error');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    useEffect(() => {
        getAllWords();
        getAllType();
        getAllWordType();
    }, []);

    function addWordTypeForm(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        const postData = {
            typeId: e.target.typeId?.value,
            wordId: e.target.wordId?.value,
            wordIds: selectedOptionMulti?.map((item) => item.value),
        };

        axios.post(`${PATH_NAME}word-type`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllWordType();
                    e.target.reset();
                    setIsOpen(false);
                    setSelectedOption(null);
                    toast.success("Muvaffaqqiyatli qo'shildi")
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            })
            .finally(() => {
                setSelectedOptionMulti([])
            })
        ;
    }

    function getDeleteWordTypeId(id) {
        setWordTypeId(id);
        setDeleteModal(true)
    }

    function deleteWordType() {
        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.delete(`${PATH_NAME}word-type/${wordTypeId}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllWordType();
                    toast.success("Muvaffaqqiyatli o'chirildi");
                    setDeleteModal(false);
                    setWordTypeId(null)
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    function openUpdateModal(id) {
        const token = localStorage.getItem('accessToken');
        setWordTypeId(id);
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}word-type/get?id=${id}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setUpdateWordTypeElement(response.data.data);
                    setUpdateModal(true);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    function updateWordType(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        const postData = {
            typeId: e.target.typeId?.value,
            wordId: e.target.wordId?.value,
            wordIds: selectedOptionMulti?.map((item) => item.value),
        };


        axios.put(`${PATH_NAME}word-type/${wordTypeId}`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllWordType();
                    e.target.reset();
                    setUpdateModal(false);
                    setUpdateWordTypeElement({});
                    setWordTypeId(null);
                    setSelectedOption(null);
                    toast.success("Muvaffaqqiyatli o'zgartirildi")
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            })
            .finally(() => {
                setSelectedOptionMulti([])
            });
    }

    const changePagination = async (page) => {
        return await axios.get(`${PATH_NAME}word-type/search-word-type?page=${page}&size=10&wordName=${wordName}`)
            .then((res) => {
                return res.data?.data
            });


    };
    const handleClick = async (event) => {
        let page = event.selected;
        const result = await changePagination(page);
        setWordsTypes(result)
    };

    function searchByWordName(e) {
        e.preventDefault();
        setWordName(e.target.wordName?.value);

        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}word-type/search-word-type?page=0&size=10&wordName=${e.target.wordName?.value}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setWordsTypes(response.data?.data)
                }
            })
            .catch((error) => {
                toast.error('Error');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    return (
        <AdminLayout>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Soʻzlarning shakl va maʻno munosabatiga koʻra turlarini belgilash</title>
            </Head>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossOrigin="anonymous"
            />
            <div className="admin-category-page">
                <div className="category-page-header">
                    <h4>Soʻzlarning shakl va maʻno munosabatiga koʻra turlarini belgilash sahifasi</h4>
                    <button onClick={toggleModal}>
                        Soʻzga so'z turi qoʻshish
                    </button>
                </div>

                <div className="card border-0">
                    <div className="card-body">
                        <div className="d-flex">
                            <form onSubmit={searchByWordName} className="d-flex" style={{width: "320px"}}>
                                <input
                                    placeholder={"soʻz nomi bo'yicha qidirish..."}
                                    name={'wordName'}
                                    className="form-control"
                                    type="search"/>
                                <button>Qidirish</button>

                            </form>

                        </div>

                        <div className="category-page-content">
                            <table className="table table-hover table-bordered mt-3">
                                <thead>
                                <tr className="table-secondary">
                                    <th>ID</th>
                                    <th>Soʻz nomi</th>
                                    <th>Tur nomi</th>
                                    <th>So'zlar</th>
                                    <th>Yaratilgan vaqt</th>
                                    <th>Harakat</th>
                                </tr>
                                </thead>
                                <tbody>
                                {wordsTypes?.content?.map((item, index) => (
                                    <tr key={index}>
                                        <td>{item.id}</td>
                                        <td>{item.wordName}</td>
                                        <td>{item.typeName}</td>
                                        <td>{item.wordIds?.map((val) => val + ',')}</td>
                                        <td>{item.createdAt?.substring(0, 10)}</td>
                                        <td>
                                            <button onClick={() => openUpdateModal(item.id)}
                                                    className="btn btn-sm btn-warning">oʻzgartirish
                                            </button>
                                            <button onClick={() => getDeleteWordTypeId(item.id)}
                                                    className="btn btn-sm btn-danger ms-2">oʻchirish
                                            </button>
                                        </td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>
                        </div>

                        <div className="row mt-2">
                            <div className="col-xl-12">
                                <div className="d-flex justify-content-center">
                                    <ReactPaginate
                                        pageCount={Math.ceil(wordsTypes?.totalElements / 10)}
                                        previousLabel={
                                            <img src="/images/arrow-prev.svg" alt=""/>
                                        }
                                        nextLabel={
                                            <img src="/images/arrow-next.svg" alt=""/>
                                        }
                                        breakLabel={"..."}
                                        marginPagesDisplayed={3}
                                        onPageChange={handleClick}
                                        containerClassName={"pagination"}
                                        pageClassName={"page-item"}
                                        pageLinkClassName={"page-link"}
                                        previousClassName={"page-item prev-page-item"}
                                        previousLinkClassName={"page-link"}
                                        nextClassName={"page-item next-page-item"}
                                        nextLinkClassName={"page-link"}
                                        breakClassName={"page-item"}
                                        breakLinkClassName={"page-link"}
                                        activeClassName={"active"}

                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <Modal isOpen={isOpen} toggle={toggleModal}>
                <ModalHeader toggle={toggleModal}>Soʻzga soʻz turi qoʻshish</ModalHeader>

                <form onSubmit={addWordTypeForm}>
                    <ModalBody>
                        <label htmlFor="wordId">Soʻz </label>

                       <div className="d-flex position-relative">
                           <Select
                               className="mb-3 w-100"
                               id="wordId"
                               name="wordId"
                               value={selectedOption}
                               onInputChange={handleInputChange}
                               onChange={handleChange}
                               options={words?.map(item => ({
                                   value: item.id,
                                   label: item.label,
                               }))}
                               isSearchable={true}
                               placeholder="kamida ikkita belgi orqali qidiring"
                           />
                           {selectedOption && (
                               <button type="button" className="btn-close reset-btn-close" onClick={handleReset}/>

                           )}
                       </div>


                        <label htmlFor="typeId">Soʻz turi</label>
                        <select required={true} name="typeId"
                                id="typeId" className="form-select mb-3">
                            <option value="" disabled={true} selected={true}>tanlang</option>
                            <>
                                {types?.map((item, index) => (
                                    <option value={item.id}>{item.name}</option>
                                ))}
                            </>
                        </select>

                        <label htmlFor="wordIds">Soʻzga soʻz turiga mos soʻzlar</label>
                       <div className="d-flex position-relative">
                           <Select
                               className="w-100"
                               id="wordIds"
                               name="wordIds"
                               isMulti={true}
                               onInputChange={handleInputChange}
                               value={selectedOptionMulti}
                               onChange={handleChangeMulti}
                               options={words?.map(item => ({
                                   value: item.id,
                                   label: item.label,
                               }))}
                               isSearchable={true}
                               placeholder="word1, word2, word3, "
                           />

                       </div>

                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleModal}>Bekor qilish</button>
                        <button type="submit" className="btn btn-success">Qoʻshish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={updateModal} toggle={toggleUpdateModal}>
                <ModalHeader toggle={toggleUpdateModal}>Tahrir qilish</ModalHeader>

                <form onSubmit={updateWordType}>
                    <ModalBody>
                        <label htmlFor="typeId">So'z turi</label>
                        <select required={true} name="typeId"
                                defaultValue={updateWordTypeElement?.typeId}
                                id="typeId" className="form-select mb-3">
                            <option value="" disabled={true} selected={true}>tanlang</option>
                            <>
                                {types?.map((item, index) => (
                                    <option value={item.id}>{item.name}</option>
                                ))}
                            </>
                        </select>

                        <label htmlFor="wordId">Soʻz</label>
                        <div className="d-flex position-relative">
                            <Select
                                id="wordId"
                                name="wordId"
                                value={updateWordTypeElement?.wordId ?
                                    wordsUpdate?.filter(word => word.id === updateWordTypeElement?.wordId)?.map(item => ({
                                        value: item.id,
                                        label: item.label,
                                    }))
                                    : selectedOption}
                                onChange={handleChange}
                                options={words?.map(item => ({
                                    value: item.id,
                                    label: item.label,
                                }))}
                                onInputChange={handleInputChange}
                                isSearchable={true}
                                className="w-100"
                                placeholder="kamida ikkita belgi orqali qidiring..."
                            />
                            {(updateWordTypeElement?.wordId || selectedOption)  && (
                                <button type="button" className="btn-close reset-btn-close" onClick={handleResetUpdate}/>

                            )}
                        </div>

                        <label htmlFor="wordIds" className="mt-3">Soʻzga soʻz turiga mos soʻzlar</label>
                        <Select
                            className=""
                            id="wordIds"
                            name="wordIds"
                            isMulti={true}
                            defaultValue={wordsUpdate ? wordsUpdate?.filter((word) => updateWordTypeElement?.wordIds?.includes(word?.label))?.map(item => ({
                                value: item.id,
                                label: item.label,
                            })) : selectedOptionMulti}
                            onChange={handleChangeMulti}
                            options={words?.map(item => ({
                                value: item.id,
                                label: item.label,
                            }))}
                            isSearchable={true}
                            onInputChange={handleInputChange}
                            placeholder="word1, word2, word3, "
                        />

                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleUpdateModal}>Bekor qilish
                        </button>
                        <button type="submit" className="btn btn-warning">Oʻzgartirish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={deleteModal} toggle={toggleDeleteModal}>
                <ModalBody>
                    <h4>Haqiqatdan ham ushbu maʻlumotni oʻchirmoqchimisiz ? </h4>
                </ModalBody>
                <ModalFooter>
                    <button type="button" className="btn btn-secondary" onClick={toggleDeleteModal}>Yoq</button>
                    <button type="submit" className="btn btn-danger" onClick={deleteWordType}>Ha</button>
                </ModalFooter>
            </Modal>
        </AdminLayout>
    );
}

export default Index;