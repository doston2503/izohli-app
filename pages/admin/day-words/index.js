import React, {useEffect, useRef, useState} from 'react';
import AdminLayout from "../../../components/AdminLayout";
import axios from "axios";
import {PATH_NAME} from "../../../src/utils/constants";
import {toast} from "react-toastify";
import Head from "next/head";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Select from "react-select";
import {useRouter} from "next/router";

function Index(props) {
    const formRef = useRef(null);
    const router = useRouter();
    const [isOpen, setIsOpen] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);
    const [searchInput, setSearchInput] = useState('');
    const [selectedOption, setSelectedOption] = useState(null);
    const [dayWords, setDayWords] = useState([]);
    const [words, setWords] = useState([]);
    const [wordsUpdate, setWordsUpdate] = useState([]);
    const [updateDayWordElement, setUpdateDayWordElement] = useState({});
    const [dayWordId, setDayWordId] = useState(null);

    const toggleModal = () => {
        setIsOpen(!isOpen);
    };
    const toggleDeleteModal = () => {
        setDeleteModal(!deleteModal);
    };
    const toggleUpdateModal = () => {
        setUpdateModal(!updateModal);
    };
    const handleChange = (selectedOption) => {
        setSelectedOption(selectedOption);
    };

    const handleInputChange = (input) => {
        setSearchInput(input);
        if (input?.length >= 2) {
            const headers = {
                headers: {
                    'Accept': 'application/json',
                },
            };
            axios.get(`${PATH_NAME}words/get-by-label-on-array?label=${input}`, headers)
                .then((response) => {
                    setWords(response.data.data);
                })
                .catch((error) => {
                    toast.error('Error');
                    console.error('Error:', error);
                });
        } else {
            setWords([])
        }

    };

    const handleReset = () => {
        setSelectedOption(null);
    };
    const handleResetUpdate = () => {
        handleChange(null);
        setSelectedOption(null);
        setUpdateDayWordElement({});
    };

    function getAllWords() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}words/get-all`, headers)
            .then((response) => {
                setWordsUpdate(response.data.data);
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    function getAllDayWords() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}day-words/get-all`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setDayWords(response.data.data);
                    formRef?.current?.reset()
                }
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getAllWords();
        getAllDayWords();
    }, []);

    function addDayWord(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        const postData = {
            date: e.target.date?.value,
            wordId: e.target.wordId?.value
        };

        axios.post(`${PATH_NAME}day-words`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllDayWords();
                    setSelectedOption(null);
                    e.target.reset();
                    setIsOpen(false);
                    toast.success("Muvaffaqqiyatli qo'shildi")
                }
                else{
                    toast.error('In this case, no new word can be added per day.')
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status===401){
                    router.push('/login')
                }
            });
    }

    function getDeleteDayWord(id) {
        setDayWordId(id);
        setDeleteModal(true)
    }

    function deleteDayWord() {
        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.delete(`${PATH_NAME}day-words/${dayWordId}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllDayWords();
                    toast.success("Muvaffaqqiyatli o'chirildi");
                    setDeleteModal(false);
                    setDayWordId(null)
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status===401){
                    router.push('/login')
                }
            });
    }

    function openUpdateModal(id) {
        const token = localStorage.getItem('accessToken');
        setDayWordId(id);
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}day-words/get?id=${id}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setUpdateDayWordElement(response.data.data);
                    setUpdateModal(true);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    function updateDayWord(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        const postData = {
            date: e.target.date?.value,
            wordId: e.target.wordId?.value

        };

        axios.put(`${PATH_NAME}day-words/${dayWordId}`, postData, headers)
            .then((response) => {

                if (response.data?.success) {
                    getAllDayWords();
                    e.target.reset();
                    setUpdateModal(false);
                    setUpdateDayWordElement({});
                    setDayWordId(null);
                    setSelectedOption(null);
                    toast.success("Muvaffaqqiyatli o'zgartirildi")
                }
                else{
                    toast.error('In this case, no new word can be added per day.')
                }

            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status===401){
                    router.push('/login')
                }
            });
    }

    function searchByDate(e) {
        e.preventDefault();

        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}day-words/get-by-date-on-array?date=${e.target.date?.value}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setDayWords(response.data?.data)
                }
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    return (
        <AdminLayout>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Kun soʻzi</title>
            </Head>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossOrigin="anonymous"
            />
            <div className="admin-category-page">
                <div className="category-page-header">
                    <h4>Kun soʻzi sahifasi</h4>

                    <div className="d-flex">
                        <button onClick={toggleModal}>
                            Kun soʻzi qoʻshish
                        </button>
                    </div>

                </div>

               <div className="card border-0">
                   <div className="card-body">
                       <div className="d-flex w-25">
                           <form onSubmit={searchByDate} className="d-flex" ref={formRef}>
                               <input
                                   name={'date'}
                                   className="form-control"
                                   type="date"/>
                               <button >Qidirish</button>
                               <button onClick={getAllDayWords} type={"button"} className="ms-2">Tozalash</button>

                           </form>

                       </div>
                       <div className="category-page-content">
                           <table className="table table-bordered table-hover mt-3">
                               <thead>
                               <tr className="table-secondary">
                                   <th>ID</th>
                                   <th>Soʻz nomi</th>
                                   <th>Vaqti</th>
                                   <th>Harakat</th>
                               </tr>
                               </thead>
                               <tbody>
                               {dayWords?.map((item, index) => (
                                   <tr key={index}>
                                       <td>{item.id}</td>
                                       <td>{item.words?.label}</td>
                                       <td>{item.date}</td>
                                       <td>
                                           <button onClick={() => openUpdateModal(item.id)}
                                                   className="btn btn-sm btn-warning">oʻzgartirish
                                           </button>
                                           <button onClick={() => getDeleteDayWord(item.id)}
                                                   className="btn btn-sm btn-danger ms-2">oʻchirish
                                           </button>
                                       </td>
                                   </tr>
                               ))}

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
            </div>


            <Modal isOpen={isOpen} toggle={toggleModal}>
                <ModalHeader toggle={toggleModal}>Kun soʻzi qoʻshish</ModalHeader>

                <form onSubmit={addDayWord}>
                    <ModalBody>
                        <label htmlFor="wordId">Soʻz</label>
                       <div className="d-flex position-relative">
                           <Select
                               className="mb-3 w-100"
                               id="wordId"
                               name="wordId"
                               value={selectedOption}
                               onChange={handleChange}
                               options={words?.map(item => ({
                                   value: item.id,
                                   label: item.label,
                               }))}
                               onInputChange={handleInputChange}
                               isSearchable={true}
                               placeholder="kamida ikkita belgi orqali qidiring..."
                           />
                           {selectedOption && (
                               <button type="button" className="btn-close reset-btn-close" onClick={handleReset}/>

                           )}
                       </div>

                        <label htmlFor="date">Vaqti</label>
                        <input type="date"
                               id="date"
                               className="form-control mb-3"
                               name="date"/>


                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleModal}>Bekor qilish</button>
                        <button type="submit" className="btn btn-success">Qoʻshish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={updateModal} toggle={toggleUpdateModal}>
                <ModalHeader toggle={toggleUpdateModal}>Kun soʻzini tahrirlash</ModalHeader>

                <form onSubmit={updateDayWord}>
                    <ModalBody>
                        <label htmlFor="wordId">Soʻz</label>
                       <div className="d-flex position-relative">
                           <Select
                               className="mb-3 w-100"
                               id="wordId"
                               name="wordId"
                               value={updateDayWordElement?.wordId ?
                                   wordsUpdate?.filter(word => word.id === updateDayWordElement?.wordId)?.map(item => ({
                                       value: item.id,
                                       label: item.label,
                                   }))
                                   : selectedOption}
                               onChange={handleChange}
                               options={words?.map(item => ({
                                   value: item.id,
                                   label: item.label,
                               }))}
                               onInputChange={handleInputChange}
                               isSearchable={true}
                               placeholder="Kamida ikkita belgi orqali qidiring"
                           />
                           {(updateDayWordElement?.wordId || selectedOption) && (
                               <button type="button" className="btn-close reset-btn-close" onClick={handleResetUpdate}/>

                           )}
                       </div>

                        <label htmlFor="date">Vaqt</label>
                        <input type="date"
                               id="date"
                               defaultValue={updateDayWordElement?.date}
                               className="form-control mb-3"
                               name="date"/>


                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleUpdateModal}>Bekor qilish</button>
                        <button type="submit" className="btn btn-warning">Oʻzgartirish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={deleteModal} toggle={toggleDeleteModal}>
                <ModalBody>
                    <h4>Haqiqatdan ham ushbu maʻlumotni o'ʻhirmoqchimisiz ?</h4>
                </ModalBody>
                <ModalFooter>
                    <button type="button" className="btn btn-secondary" onClick={toggleDeleteModal}>Yoq</button>
                    <button type="submit" className="btn btn-danger" onClick={deleteDayWord}>Ha</button>
                </ModalFooter>
            </Modal>

        </AdminLayout>
    );
}

export default Index;