import React, {useEffect, useMemo, useRef, useState} from 'react';
import AdminLayout from "../../../components/AdminLayout";
import axios from "axios";
import {PATH_NAME} from "../../../src/utils/constants";
import {toast} from "react-toastify";
import Head from "next/head";
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Select from 'react-select';
import {useRouter} from "next/router";
import ReactPaginate from "react-paginate";
import TinyMCEEditor from "../../../components/TinyMCEEditor";

function Index(props) {
    const [editorContentTitle, setEditorContentTitle] = useState('');
    const [editorContentDesc, setEditorContentDesc] = useState('');
    const [editorContentSource, setEditorContentSource] = useState('');

    const handleEditorChangeTitles = (content, editor) => {
        setEditorContentTitle(content);
    };
    const handleEditorChangeDesc = (content, editor) => {
        setEditorContentDesc(content);
    };
    const handleEditorChangeSource = (content, editor) => {
        setEditorContentSource(content);
    };


    const router = useRouter();
    const [isOpen, setIsOpen] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);

    const [selectedOption, setSelectedOption] = useState(null);
    const [notes, setNotes] = useState([]);
    const [words, setWords] = useState([]);
    const [updateNoteElement, setUpdateNoteElement] = useState({});
    const [noteId, setNoteId] = useState(null);
    const [title, setTitle] = useState("");
    const [searchInput, setSearchInput] = useState('');
    const [wordsUpdate, setWordsUpdate] = useState([]);

    const toggleModal = () => {
        setIsOpen(!isOpen);
    };
    const toggleDeleteModal = () => {
        setDeleteModal(!deleteModal);
    };
    const toggleUpdateModal = () => {
        setUpdateModal(!updateModal);
    };
    const handleChange = (selectedOption) => {
        setSelectedOption(selectedOption);
    };

    const handleInputChange = (input) => {
        setSearchInput(input);
        if (input?.length >= 2) {
            const headers = {
                headers: {
                    'Accept': 'application/json',
                },
            };
            axios.get(`${PATH_NAME}words/get-by-label-on-array?label=${input}`, headers)
                .then((response) => {
                    setWords(response.data.data);
                })
                .catch((error) => {
                    toast.error('Error');
                    console.error('Error:', error);
                });
        } else {
            setWords([])
        }

    };

    const handleReset = () => {
        setSelectedOption(null);
    };
    const handleResetUpdate = () => {
        handleChange(null);
        setSelectedOption(null);
        setUpdateNoteElement({});
    };


    function getAllWords() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}words/get-all`, headers)
            .then((response) => {
                setWordsUpdate(response.data.data);
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    function getAllNotes() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}note/get-all`, headers)
            .then((response) => {
                console.log(response);
                if (response.data?.success) {
                    setNotes(response.data.data);
                }
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getAllWords();
        getAllNotes();
    }, []);

    function addNoteForm(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        const postData = {
            wordId: e.target.wordId?.value,
            titles: e.target.titles?.value,
            descriptions: editorContentDesc,
            sources: editorContentSource
        };

        axios.post(`${PATH_NAME}note`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllNotes();
                    setSelectedOption(null);
                    e.target.reset();
                    setIsOpen(false);
                    toast.success("Muvaffaqqiyatli qo'shildi")
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    function getDeleteNoteId(id) {
        setNoteId(id);
        setDeleteModal(true);
    }

    function deleteNote() {
        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.delete(`${PATH_NAME}note/${noteId}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllNotes();
                    toast.success("Muvaffaqqiyatli o'chirildi");
                    setDeleteModal(false);
                    setNoteId(null)
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    function openUpdateModal(id) {
        const token = localStorage.getItem('accessToken');
        setNoteId(id);
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}note/get?id=${id}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setUpdateNoteElement(response.data.data);
                    setUpdateModal(true);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    function updateNote(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const postData = {
            titles: e.target.titles?.value,
            descriptions: editorContentDesc,
            sources: editorContentSource,
            wordId: e.target.wordId?.value

        };
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };

        axios.put(`${PATH_NAME}note/get/${noteId}`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllNotes();
                    e.target.reset();
                    setUpdateModal(false);
                    setUpdateNoteElement({});
                    setNoteId(null);
                    setSelectedOption(null);
                    toast.success("Muvaffaqqiyatli o'zgartirildi")
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    const changePagination = async (page) => {
        return await axios.get(`${PATH_NAME}note/search-note?page=${page}&size=10&title=${title}`)
            .then((res) => {
                return res.data?.data
            });


    };
    const handleClick = async (event) => {
        let page = event.selected;
        const result = await changePagination(page);
        setNotes(result)
    };

    function searchByTitle(e) {
        e.preventDefault();
        setTitle(e.target.title?.value);

        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}note/search-note?page=0&size=10&title=${e.target.title?.value}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setNotes(response.data?.data)
                }
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    return (
        <AdminLayout>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Soʻz uchun qoʻshimcha maʻlumotlar</title>
            </Head>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossOrigin="anonymous"
            />
            <div className="admin-category-page">
                <div className="category-page-header">
                    <h4>Qoʻshimcha maʻlumotlar sahifasi</h4>
                    <button onClick={toggleModal}>
                        Qoʻshimcha maʻlumot qoʻshish
                    </button>
                </div>

                <div className="card border-0">
                    <div className="card-body">
                        <div className="d-flex">
                            <form onSubmit={searchByTitle} className="d-flex" style={{width: '320px'}}>
                                <input
                                    placeholder={"sarlavha bo'yicha qidirish"}
                                    name={'title'}
                                    className="form-control"
                                    type="search"/>
                                <button>Qidirish</button>

                            </form>

                        </div>

                        <div className="category-page-content">
                            <table className="table table-hover table-bordered mt-3">
                                <thead>
                                <tr className="table-secondary " style={{verticalAlign: 'middle'}}>
                                    <th>ID</th>
                                    <th>Sarlavha</th>
                                    <th>Tavsif</th>
                                    <th>Manba</th>
                                    <th>Soʻz nomi</th>
                                    <th>Yaratilgan vaqt</th>
                                    <th>Harakat</th>
                                </tr>
                                </thead>
                                <tbody>
                                {notes?.content?.map((item, index) => (
                                    <tr key={index}>
                                        <td>{item.id}</td>
                                        <td style={{width: "200px"}}>
                                            {item.titles}
                                        </td>
                                        <td style={{width: "250px"}}>
                                            <div dangerouslySetInnerHTML={{__html: item.descriptions}}/>
                                        </td>
                                        <td>
                                            <div dangerouslySetInnerHTML={{__html: item.sources}}/>
                                        </td>
                                        <td>{item.wordName}</td>
                                        <td>{item.createdAt?.substring(0, 10)}</td>
                                        <td >
                                            <div className="d-flex">
                                                <button onClick={() => openUpdateModal(item.id)}
                                                        className="btn btn-sm btn-warning">oʻzgartirish
                                                </button>
                                                <button onClick={() => getDeleteNoteId(item.id)}
                                                        className="btn btn-sm btn-danger ms-2">oʻchirish
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>
                        </div>

                        <div className="row mt-2">
                            <div className="col-xl-12">
                                <div className="d-flex justify-content-center">
                                    <ReactPaginate
                                        pageCount={Math.ceil(notes?.totalElements / 10)}
                                        previousLabel={
                                            <img src="/images/arrow-prev.svg" alt=""/>
                                        }
                                        nextLabel={
                                            <img src="/images/arrow-next.svg" alt=""/>
                                        }
                                        breakLabel={"..."}
                                        marginPagesDisplayed={3}
                                        onPageChange={handleClick}
                                        containerClassName={"pagination"}
                                        pageClassName={"page-item"}
                                        pageLinkClassName={"page-link"}
                                        previousClassName={"page-item prev-page-item"}
                                        previousLinkClassName={"page-link"}
                                        nextClassName={"page-item next-page-item"}
                                        nextLinkClassName={"page-link"}
                                        breakClassName={"page-item"}
                                        breakLinkClassName={"page-link"}
                                        activeClassName={"active"}

                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <Modal isOpen={isOpen} toggle={toggleModal} id="modal-dialogTiny">
                <ModalHeader toggle={toggleModal}>Qoʻshimcha maʻlumot qoʻshish</ModalHeader>

                <form onSubmit={addNoteForm}>
                    <ModalBody>
                        <div className="row">
                            <div className="col-xl-6">
                                <label htmlFor="wordId">Soʻz</label>
                                <div className="d-flex position-relative">
                                    <Select
                                        className="mb-3 w-100"
                                        id="wordId"
                                        name="wordId"
                                        value={selectedOption}
                                        onChange={handleChange}
                                        options={words?.map(item => ({
                                            value: item.id,
                                            label: item.label,
                                        }))}
                                        isSearchable={true}
                                        onInputChange={handleInputChange}
                                        placeholder="Kamida ikkita belgi orqali qidiring..."
                                    />
                                    {selectedOption && (
                                        <button type="button" className="btn-close reset-btn-close"
                                                onClick={handleReset}/>

                                    )}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <label htmlFor="titles">Sarlavha</label>
                                <input
                                          id="titles"
                                          className="form-control mb-3"
                                          name="titles"/>
                                {/*<TinyMCEEditor*/}
                                {/*    id="titles"*/}
                                {/*    initialValue=""*/}
                                {/*    handleEditorChange={handleEditorChangeTitles}*/}
                                {/*/>*/}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-6">
                                <label htmlFor="descriptions" className="mt-3">Tavsif</label>
                                {/*<textarea name="descriptions" id="descriptions"*/}
                                {/*          className="form-control mb-3"*/}
                                {/*          cols="30" rows="4"/>*/}
                                <TinyMCEEditor
                                    id="descriptions"
                                    initialValue=""
                                    handleEditorChange={handleEditorChangeDesc}
                                />
                            </div>
                            <div className="col-xl-6">
                                <label htmlFor="sources" className="mt-3">Manba</label>
                                {/*<textarea rows={3}*/}
                                {/*          id="sources"*/}
                                {/*          className="form-control mb-3"*/}
                                {/*          name="sources"/>*/}
                                <TinyMCEEditor
                                    id="sources"
                                    initialValue=""
                                    handleEditorChange={handleEditorChangeSource}
                                />
                            </div>
                        </div>

                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleModal}>Bekor qilish</button>
                        <button type="submit" className="btn btn-success">Qoʻshish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={updateModal} toggle={toggleUpdateModal} id="modal-dialogTiny">
                <ModalHeader toggle={toggleUpdateModal}>Tahrir qilish</ModalHeader>

                <form onSubmit={updateNote}>
                    <ModalBody>
                        <div className="row">
                            <div className="col-xl-6">
                                <label htmlFor="wordId">Soʻz</label>
                                <div className="d-flex position-relative">
                                    <Select
                                        className="mb-3 w-100"
                                        id="wordId"
                                        name="wordId"
                                        value={updateNoteElement?.wordId ? wordsUpdate?.filter(word => word.id === updateNoteElement?.wordId)?.map(item => ({
                                                value: item.id,
                                                label: item.label,
                                            }))
                                            : selectedOption}
                                        onChange={handleChange}
                                        options={words?.map(item => ({
                                            value: item.id,
                                            label: item.label,
                                        }))}
                                        isSearchable={true}
                                        onInputChange={handleInputChange}
                                        placeholder="Kamida ikkita belgi orqali qidiring..."
                                    />
                                    {(updateNoteElement?.wordId || selectedOption) && (
                                        <button type="button" className="btn-close reset-btn-close"
                                                onClick={handleResetUpdate}/>

                                    )}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <label htmlFor="titles">Sarlavha</label>
                                <input
                                    defaultValue={updateNoteElement?.titles}
                                    id="titles"
                                    className="form-control mb-3"
                                    name="titles"/>
                                {/*<TinyMCEEditor*/}
                                {/*    id="titles"*/}
                                {/*    initialValue={updateNoteElement?.titles}*/}
                                {/*    handleEditorChange={handleEditorChangeTitles}*/}
                                {/*/>*/}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-6">
                                <label htmlFor="descriptions">Tavsif</label>
                                <TinyMCEEditor
                                    id="descriptions"
                                    initialValue={updateNoteElement?.descriptions}
                                    handleEditorChange={handleEditorChangeDesc}
                                />
                            </div>
                            <div className="col-xl-6">
                                <label htmlFor="sources">Manba</label>
                                <TinyMCEEditor
                                    id="sources"
                                    initialValue={updateNoteElement?.sources}
                                    handleEditorChange={handleEditorChangeSource}/>
                            </div>
                        </div>

                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleUpdateModal}>Bekor qilish
                        </button>
                        <button type="submit" className="btn btn-warning">Oʻzgartirish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={deleteModal} toggle={toggleDeleteModal}>
                <ModalBody>
                    <h4>Haqiqatdan ham ushbu maʻlumotni oʻchirmoqchimisiz ?</h4>
                </ModalBody>
                <ModalFooter>
                    <button type="button" className="btn btn-secondary" onClick={toggleDeleteModal}>Yoq</button>
                    <button type="submit" className="btn btn-danger" onClick={deleteNote}>Ha</button>
                </ModalFooter>
            </Modal>
        </AdminLayout>
    );
}

export default Index;