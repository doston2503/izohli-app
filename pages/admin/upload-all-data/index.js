import React from 'react';
import AdminLayout from "../../../components/AdminLayout";
import Head from "next/head";
import axios from "axios";
import {PATH_NAME} from "../../../src/utils/constants";
import {toast} from "react-toastify";
import {useRouter} from "next/router";

function Index(props) {
    const router = useRouter();

    function uploadAllData(e) {
        e.preventDefault();
        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'multipart/form-data',
            },
        };
        const formData = new FormData();
        formData.append('file', event.target.files[0]);
        axios.post(`${PATH_NAME}excel/upload-all-data`, formData, headers)
            .then((response) => {
                 if (response.data?.success) {
                     toast.success("Muvaffaqqiyatli yuklandi")
                 }
                 else {
                     toast.error(response?.data?.message)
                 }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status === 401) {
                    router.push('/login')
                }
            });
    }

    return (
        <AdminLayout>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Barcha maʻlumotlarni excel fayl koʻrinishida yuklash</title>
            </Head>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossOrigin="anonymous"
            />
            <div className="admin-category-page">
                <div className="category-page-header">
                    <h4>Barcha maʻlumotlarni excel fayl ko'rinishida yuklash sahifasi</h4>
                </div>

                <div className="upload-all-data">

                    <div className="card  border-0">
                        <div className="card-body">
                                <div className="d-flex justify-content-between mb-3">
                                    <h4>Excel fayl yuklash</h4>
                                    <a href="/example_word.xlsx" download={true}>Namuna sifatidagi excel faylni yuklab olish</a>
                                </div>
                                <label htmlFor="file">
                                    <img src="/images/upload.svg" alt=""/>
                                    <p className="mb-0">
                                        Fayllarni shu yerga tashlang yoki yuklash uchun bosing.
                                    </p>
                                    <input type="file"
                                           id="file"
                                           name="file"
                                           onChange={uploadAllData}
                                           className=""/>
                                </label>

                        </div>

                    </div>
                </div>
            </div>
        </AdminLayout>
    );
}

export default Index;