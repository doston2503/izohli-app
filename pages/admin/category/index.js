import React, {useEffect, useState} from 'react';
import AdminLayout from "../../../components/AdminLayout";
import Head from "next/head";
import {toast} from "react-toastify";
import {PATH_NAME} from "../../../src/utils/constants";
import axios from 'axios';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {useRouter} from "next/router";
function Index(props) {
    const router = useRouter();
    const [isOpen, setIsOpen] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [updateModal, setUpdateModal] = useState(false);

    const [categories, setCategories] = useState([]);
    const [updateCategoryElement, setUpdateCategoryElement] = useState({});
    const [categoryId, setCategoryId] = useState(null);

    const toggleModal = () => {
        setIsOpen(!isOpen);
    };
    const toggleDeleteModal = () => {
        setDeleteModal(!deleteModal);
    };
    const toggleUpdateModal = () => {
        setUpdateModal(!updateModal);
    };

    function getAllCategories() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}category/get-all`, headers)
            .then((response) => {
                setCategories(response.data.data);
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getAllCategories()
    }, []);

    function addCategoryForm(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`

            },
        };
        const postData = {
            names: e.target.names?.value,
            descriptions: e.target.descriptions?.value,
        };

        axios.post(`${PATH_NAME}category`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllCategories();
                    e.target.reset();
                    setIsOpen(false);
                    toast.success("Kategoriya qo'shildi")
                }

            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status===401){
                    router.push('/login')
                }
            });
    }

    function getDeleteCategoryId(id) {
        setCategoryId(id);
        setDeleteModal(true)
    }

    function deleteCategory() {
        const token = localStorage.getItem('accessToken');
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.delete(`${PATH_NAME}category/${categoryId}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllCategories();
                    toast.success("Kategoriya o'chirildi");
                    setDeleteModal(false)
                }
            })
            .catch((error) => {
                toast.error('Error');
                if (error.response?.status===401){
                    router.push('/login')
                }
            });
    }

    function openUpdateModal(id) {
        const token = localStorage.getItem('accessToken');
        setCategoryId(id);
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}category/get?id=${id}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setUpdateCategoryElement(response.data.data);
                    setUpdateModal(true);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    function updateCategory(e) {
        const token = localStorage.getItem('accessToken');
        e.preventDefault();
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        const postData = {
            names: e.target.names?.value,
            descriptions: e.target.descriptions?.value,
        };

        axios.put(`${PATH_NAME}category/${categoryId}`, postData, headers)
            .then((response) => {
                if (response.data?.success) {
                    getAllCategories();
                    e.target.reset();
                    setUpdateModal(false);
                    setUpdateCategoryElement({});
                    toast.success("Kategoriya o'zgartirildi")
                }
            })
            .catch((error) => {
                toast.error('Xatolik');
                if (error.response?.status===401){
                    router.push('/login')
                }
            });
    }


    return (
        <AdminLayout>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Admin kategoriya</title>
            </Head>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossOrigin="anonymous"
            />
            <div className="admin-category-page">
                <div className="category-page-header">
                    <h4>Kategoriya sahifasi</h4>
                    <button onClick={toggleModal}>
                        Kategoriya qoʻshish
                    </button>
                </div>
                <div className="card border-0">
                    <div className="card-body">
                        <div className="category-page-content">
                            <table className="table table-bordered table-hover mt-3">
                                <thead>
                                <tr className="table-secondary">
                                    <th>ID</th>
                                    <th>Nomi</th>
                                    <th>Tavsif</th>
                                    <th>Harakat</th>
                                </tr>
                                </thead>
                                <tbody>
                                {categories?.map((item, index) => (
                                    <tr key={item.id}>
                                        <td>{item.id}</td>
                                        <td>{item.names}</td>
                                        <td>{item.descriptions}</td>
                                        <td>
                                            <button onClick={() => openUpdateModal(item.id)}
                                                    className="btn btn-sm btn-warning">oʻzgartirish
                                            </button>
                                            <button onClick={() => getDeleteCategoryId(item.id)}
                                                    className="btn btn-sm btn-danger ms-2">oʻchirish
                                            </button>
                                        </td>
                                    </tr>
                                ))}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


            <Modal isOpen={isOpen} toggle={toggleModal}>
                <ModalHeader toggle={toggleModal}>Kategoriya qoʻshish</ModalHeader>

                <form onSubmit={addCategoryForm}>
                    <ModalBody>
                        <label htmlFor="names">Nomi</label>
                        <input type="text"
                               required={true}
                               id={'names'}
                               name={'names'}
                               className="form-control mb-3 mt-1"
                        />

                        <label htmlFor="descriptions">Tavsifi</label>
                        <textarea name="descriptions"
                                  id="descriptions"
                                  className="form-control"
                                  cols="30" rows="3"
                        />
                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleModal}>Bekor qilish</button>
                        <button type="submit" className="btn btn-success">Qoʻshish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={updateModal} toggle={toggleUpdateModal}>
                <ModalHeader toggle={toggleUpdateModal}>Kategoriyani tahrir qilish</ModalHeader>

                <form onSubmit={updateCategory}>
                    <ModalBody>
                        <label htmlFor="names">Nomi</label>
                        <input type="text"
                               defaultValue={updateCategoryElement?.names}
                               required={true}
                               id={'names'}
                               name={'names'}
                               className="form-control mb-3 mt-1"
                        />

                        <label htmlFor="descriptions">Tavsifi</label>
                        <textarea name="descriptions"
                                  defaultValue={updateCategoryElement?.descriptions}
                                  id="descriptions"
                                  className="form-control"
                                  cols="30" rows="3"
                        />
                    </ModalBody>

                    <ModalFooter>
                        <button type="button" className="btn btn-danger" onClick={toggleUpdateModal}>Bekor qilish</button>
                        <button type="submit" className="btn btn-warning">Oʻzgartirish</button>
                    </ModalFooter>
                </form>

            </Modal>

            <Modal isOpen={deleteModal} toggle={toggleDeleteModal}>
                <ModalBody>
                    <h4>Haqiqatan ham ushbu maʻlumotlarni oʻchirib tashlamoqchimisiz?</h4>
                </ModalBody>
                <ModalFooter>
                    <button type="button" className="btn btn-secondary" onClick={toggleDeleteModal}>Yoq</button>
                    <button type="submit" className="btn btn-danger" onClick={deleteCategory}>Ha</button>
                </ModalFooter>
            </Modal>
        </AdminLayout>
    );
}

export default Index;