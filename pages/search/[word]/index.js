import React, {useEffect, useState} from 'react';
import Link from "next/link";
import Head from "next/head";
import axios from "axios";
import {formatDate, PATH_NAME} from "../../../src/utils/constants";
import {toast} from "react-toastify";
import {useRouter} from "next/router";
import {getWords, getWordsByLabel, updateState} from "../../../redux/actions/mainAction";
import {connect} from "react-redux";

function Index(props) {
    const router = useRouter();
    // const [selectedWord, setSelectedWord] = useState('');
    // const [words, setWords] = useState([]);
    const [dayWord, setDayWord] = useState({});
    const [wordLength, setWordLength] = useState(0);
    const [formattedDate, setFormattedDate] = useState('');

    function getAllWords() {
        const headers = {
            headers: {
                'Accept': 'application/json',
            },
        };
        axios.get(`${PATH_NAME}words/get-all-word-search?latter=&page=0&size=10`, headers)
            .then((response) => {
                setWordLength(response.data?.data?.totalElement)
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    function getAllDayWords() {
        const token = localStorage.getItem('accessToken');
        const currentDay = new Date().toISOString().substring(0, 10);
        const headers = {
            headers: {
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        };
        axios.get(`${PATH_NAME}day-words/get-by-date?date=${currentDay}`, headers)
            .then((response) => {
                if (response.data?.success) {
                    setDayWord(response.data.data);
                    setFormattedDate(formatDate(response?.data?.data?.date));
                }
            })
            .catch((error) => {
                toast.error('Error');
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        getAllDayWords();
        getAllWords();
        // getAllWord()
        props.getWordsByLabel(router?.query?.word, true);
        props.updateState({searchWord: router?.query?.word})
    }, [router?.query?.word]);

    return (
        <>
            <Head>
                <title>{props.wordsPage?.length < 1 ? "Izohli" :
                    props.wordsPage && props.wordsPage[0]?.label} | Izohli</title>
                {/*<meta name="description" content={props.wordsPage?.length < 1 ? "" :*/}
                {/*    props.wordsPage &&*/}
                {/*    props.wordsPage[0]?.notes[0]?.titles + " " + props.wordsPage[0]?.notes[0]?.descriptions} />*/}
                <meta name="description" content={props.wordsPage?.length < 1 ? "" :
                    props.wordsPage &&
                    props.wordsPage[0]?.label}/>
            </Head>
            <script
                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                crossOrigin="anonymous"
            />
            <div className="search-response-component">
                {props.wordsPage?.length < 1 ?
                    <h1 className="text-center mb-5" style={{marginTop: '100px'}}>Siz izlagan soʻz bizda mavjud
                        emas</h1> :

                    <div className={props.wordsPage?.length > 1 ? "search-response-card" : 'search-response-card-one'}>
                        {props.wordsPage?.map((item, index) => (
                            <>
                                <div className="response-header">
                                    <div className="response-word">
                                        {item?.label}
                                        {/*<img src="/images/voice.svg" alt="" style={{cursor: 'pointer'}}/>
                                        <img src="/images/star.svg" alt=""/>*/}
                                    </div>
                                    <div className="word-accent">
                                        [ {item?.transcript} ] <i>{item?.categoryName}</i>
                                    </div>


                                </div>

                                <div className="response-body mt-3">
                                    <div className="comment-text">
                                        Siz tanlagan soʻz uchun quyidagi izoh topildi.
                                    </div>
                                    {item?.notes?.map((item2, index2) => (
                                        <div className="text" key={index2}>
                                                    <span className="d-flex">{index2 + 1}.
                                                        <div className="ms-1" dangerouslySetInnerHTML={{__html: item2?.titles }}/>
                                                        </span>
                                            <div style={{marginLeft: "28px"}}>
                                                {/*<div dangerouslySetInnerHTML={{__html: item2?.descriptions ? item2?.descriptions : ''}}/>*/}

                                            </div>

                                            <div style={{marginLeft: "28px"}}>
                                                <div className="source">Manba</div>
                                                <div className="source-text">
                                                    <i dangerouslySetInnerHTML={{__html: item2?.sources}}/>
                                                </div>
                                            </div>

                                        </div>
                                    ))}

                                    <hr/>
                                </div>

                                <div className="btn-group d-block">
                                    {item?.wordTypes?.map((type, index) => (
                                        <div className="d-flex align-items-center mb-3" key={index}>
                                            <b className="d-block"
                                               style={{fontSize: '18px'}}>{type?.typeName}</b>:
                                            {type?.wordIds?.map((word, wordIndex) => (
                                                <div key={wordIndex} className="ms-2">
                                                    <a className="text-decoration-none"
                                                       style={{fontSize: '18px'}}
                                                       target="_blank"
                                                       href={`/search/${word}`}>{word}</a>
                                                    {wordIndex === type?.wordIds?.length - 1 ? null : ','}
                                                </div>
                                            ))}</div>
                                    ))}
                                </div>
                            </>
                        ))}
                    </div>

                }
            </div>
        </>
    );
}

const mapStateToProps = state => {
    return {
        isLoading: state.main.isLoading,
        words: state.main.words,
        wordsPage: state.main.wordsPage,
    }
};

export default connect(mapStateToProps, {updateState, getWords, getWordsByLabel})(Index);