import { Html, Head, Main, NextScript } from 'next/document'
import React from "react";

export default function Document() {
    return (
        <Html>
            <Head>
                <link rel="icon" href="/images/icon.svg" />
                {/* General SEO */}
                <meta name="description" content="Oʻzbek tilining izohli lugʻati" />
                <meta name="robots" content="index, follow"/>
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8"/>
                <meta name="language" content="uzbek"/>
                <meta name="revisit-after" content="1 days"/>
                <meta name="title" content="Izohli - Oʻzbek tilining izohli lugʻati"/>

                {/* Facebook */}
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Izohli - Oʻzbek tilining izohli lugʻati" />
                <meta property="og:description" content="Oʻzbek tilining izohli lugʻati" />
                <meta property="og:image" content="/images/icon.svg" />

                {/* Twitter */}
                <meta name="twitter:title" content="Izohli - Oʻzbek tilining izohli lugʻati" />
                <meta name="twitter:description" content="Oʻzbek tilining izohli lugʻati" />
                <meta name="twitter:image" content="/images/icon.svg" />

                {/* Telegram */}
                <meta property="og:telegram" content="true" />
            </Head>
            <body>
            <Main />
            <NextScript />
            </body>
        </Html>
    )
}