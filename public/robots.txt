# *
User-agent: *
Allow: /

# Host
Host: http://izohli.uz/

# Sitemaps
Sitemap: http://izohli.uz/sitemap.xml
